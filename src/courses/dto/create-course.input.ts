import { InputType, Int, Field } from '@nestjs/graphql';
import { CourseCategory } from '../enums/course-category.enum';

@InputType()
export class CreateCourseInput {

  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => String, { nullable: false })
  description: string;

  @Field(() => Int, { nullable: false })
  duration: number;

  @Field(() => CourseCategory)
  category: CourseCategory;

  @Field(() => String, { nullable: false })
  image: string;

  @Field(() => String, { nullable: false })
  tutor_id: string;

}
