import { Test, TestingModule } from '@nestjs/testing';
import { CoursesService } from './courses.service';
import { CourseCategory } from './enums/course-category.enum';

describe('CoursesService', () => {
  let service: CoursesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoursesService],
    }).compile();

    service = module.get<CoursesService>(CoursesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should create a new course', async () => {
    const newCourse = await service.create({
      name: "Curso Teste 1",
      description: "Este é um curso de teste",
      duration: 120,
      category: CourseCategory.ESTADUAL,
      image: "https://guiadoestudante.abril.com.br/wp-content/uploads/sites/4/2021/04/Curso-online.jpg",
      tutor_id: "tutor_01"
    });

    expect(newCourse).toHaveProperty('id');
  });
});
