import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { ObjectType, Field, Int } from '@nestjs/graphql';
import { CourseCategory } from '../enums/course-category.enum';

@ObjectType()
@Entity()
export class Course {
  @Field(() => Int, { description: 'Chave primária', nullable: false })
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 100})
  name: string;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 255})
  description: string;

  @Field(() => Int, { nullable: false })
  @Column("int")
  duration: number;

  @Field(() => CourseCategory)
  @Column("enum", {enum: CourseCategory})
  category: CourseCategory;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 255})
  image: string;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 255})
  tutor_id: string;
}
