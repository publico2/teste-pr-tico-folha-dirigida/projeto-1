import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { CreateCourseInput } from './dto/create-course.input';
import { UpdateCourseInput } from './dto/update-course.input';
import { Course } from './entities/course.entity';

@Injectable()
export class CoursesService {

  constructor(
    @InjectRepository(Course) private courseRepository: Repository<Course>
  ) { }

  async create(createCourseInput: CreateCourseInput): Promise<Course> {
    const newCourse = this.courseRepository.create(createCourseInput);
    return await this.courseRepository.save(newCourse);
  }

  async findAll(options?: FindManyOptions<Course>): Promise<Course[]> {
    return await this.courseRepository.find(options);
  }

  async findOne(id: number, options?: FindManyOptions<Course>): Promise<Course> {
    return await this.courseRepository.findOne(id, options);
  }

  async update(id: number, updateCourseInput: UpdateCourseInput): Promise<Course> {
    return await this.courseRepository.manager.transaction(async transaction => {
      const course = await transaction.findOne(Course, id);
      if (!course) throw new Error(`Course not found`);
      Object.assign(course, updateCourseInput);
      const updatedCourse = await transaction.save(course);
      return updatedCourse;
    });
  }

  async remove(id: number): Promise<Course> {
    return await this.courseRepository.manager.transaction(async transaction => {
      const course = await transaction.findOne(Course, id);
      if (!course) throw new Error(`Course not found`);
      const removed = await transaction.remove(course);
      removed.id = id;
      return removed;
    });
  }
}
