import { Resolver, Query, Mutation, Args, Int, Info } from '@nestjs/graphql';
import { CoursesService } from './courses.service';
import { Course } from './entities/course.entity';
import { CreateCourseInput } from './dto/create-course.input';
import { UpdateCourseInput } from './dto/update-course.input';
import { GraphQLResolveInfo } from 'graphql';
import fieldsList from '../helpers/fieldsList';

@Resolver(() => Course)
export class CoursesResolver {
  constructor(private readonly coursesService: CoursesService) {}

  @Mutation(() => Course)
  createCourse(@Args('createCourseInput') createCourseInput: CreateCourseInput) {
    return this.coursesService.create(createCourseInput);
  }

  @Query(() => [Course], { name: 'courses' })
  findAll(
    @Info() info: GraphQLResolveInfo
  ) {
    const attributes = fieldsList(info);
    return this.coursesService.findAll({select: attributes});
  }

  @Query(() => Course, { name: 'course' })
  findOne(
    @Info() info: GraphQLResolveInfo,
    @Args('id', { type: () => Int }) id: number
  ) {
    const attributes = fieldsList(info);
    return this.coursesService.findOne(id, {select: attributes});
  }

  @Mutation(() => Course)
  updateCourse(
    @Args('id', {type: () => Int}) id: number, 
    @Args('updateCourseInput') updateCourseInput: UpdateCourseInput
  ) {
    return this.coursesService.update(id, updateCourseInput);
  }

  @Mutation(() => Course)
  removeCourse(@Args('id', { type: () => Int }) id: number) {
    return this.coursesService.remove(id);
  }
}
