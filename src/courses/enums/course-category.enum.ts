import { registerEnumType } from "@nestjs/graphql";

export enum CourseCategory {
    ESTADUAL = 'estadual',
    MUNICIPAL = 'municipal',
    FEDERAL = 'federal'
}

registerEnumType(CourseCategory, {
    name: 'CourseCategory',
});