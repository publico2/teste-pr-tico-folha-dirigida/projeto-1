import { GraphQLResolveInfo } from "graphql";
import { fieldsList as _fieldsList } from "graphql-fields-list";

export default function fieldsList<T>(info: GraphQLResolveInfo): (keyof T)[] {
    return _fieldsList(info) as  (keyof T)[];
}