import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Course } from "../courses/entities/course.entity";

const databaseConfig: TypeOrmModuleOptions = {
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "admin",
    database: "folha-dirigida-projeto1",
    synchronize: true,
    entities: [Course],
    logging: true
};

export default databaseConfig;